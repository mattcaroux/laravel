# **Mon premier projet Laravel - Blog simple**
Bienvenue dans mon premier projet Laravel ! Ce projet a été créé dans le but de découvrir Laravel, un framework PHP puissant et populaire utilisé pour le développement web.

## **Objectif du projet**
L'objectif principal de ce projet est de me familiariser avec Laravel et d'explorer les différentes fonctionnalités et concepts qu'il offre. En créant un simple blog, je peux me concentrer sur l'apprentissage de Laravel sans la complexité supplémentaire d'un projet plus avancé.

## **Fonctionnalités du blog**
Ce blog simple propose une fonctionnalité de consultation d'articles. Les visiteurs peuvent naviguer sur le site et cliquer sur les liens des articles pour les consulter. Cependant, il n'y a pas d'autres fonctionnalités avancées telles que la possibilité de créer un compte utilisateur, de publier des articles ou de laisser des commentaires. Le but ici est de se concentrer sur l'apprentissage de Laravel et d'acquérir les compétences de base pour la création d'une application web. Mais je pourrais peut être dans une autre version faire ses améliorations

## **Apprentissage de Laravel**
En développant ce projet, j'ai l'occasion d'apprendre plusieurs aspects de Laravel, notamment :

1. **Structure MVC** : Laravel suit l'architecture Modèle-Vue-Contrôleur, qui permet une organisation claire et cohérente du code. J'ai eu l'opportunité d'apprendre aussi cette structure avec JavaFX mais il y a tout de même de nombreuse différence 

2. **Routes** : Je vais découvrir comment définir des routes pour gérer les différentes actions dans mon application web.

3. **Contrôleurs** : Je vais apprendre à créer des contrôleurs pour traiter les requêtes des utilisateurs et gérer la logique de mon application.

4. **Vues** : Je vais utiliser les vues de Laravel pour afficher les données de manière conviviale et créer une interface utilisateur agréable.

5. **Base de données et migrations** : J'aurai l'occasion d'explorer l'utilisation de la base de données avec Laravel et d'apprendre comment gérer les migrations pour maintenir une structure de base de données cohérente. J'utilise phpMyAdmin afin de gerer simplement la liaison entre la base de donnée et mon modèle.

6. Gestion des dépendances : Laravel utilise Composer pour gérer les dépendances du projet, ce qui facilite l'ajout de fonctionnalités supplémentaires à mon application.

## **Installation et exécution du projet**
Pour exécuter ce projet sur votre propre environnement de développement, suivez les étapes ci-dessous :

### **Installation de XAMPP et Composer**

Ses deux outils sont très utile pour le développement de ce projet car il va permettre de gérer à la fois php avec laravel mais aussi les bases de donnée qui seront sous MySql.

Pour **XAMPP** prenez la derniere version :

https://www.apachefriends.org/fr/download.html

Pour **Composer** allez sur le lien suivant :

https://www.apachefriends.org/fr/download.html

1. Assurez-vous d'avoir XAMPP ou tout autre serveur web local compatible avec PHP installé sur votre machine.

2. Clonez le projet à l'aide de la commande suivante :

```bash
git clone <https://gitlab.com/mattcaroux/laravel.git>
```
3. Accédez au répertoire du projet :

```bash
cd laravel
```

4. Installez les dépendances nécessaires à l'aide de Composer :

```bash
composer install
```

5. Renommez le fichier .env.example en .env et configurez les informations de la base de données dans ce fichier.

6. Générez une clé d'application unique pour votre projet Laravel :

```bash
php artisan key:generate
```


7. Exécutez les migrations pour créer les tables de la base de données :

```
php artisan migrate
```

Enfin, démarrez le serveur de développement de Laravel :

Pour cela vous pouvez utiliser **Apach** qui est intégré a XAMPP

Vous pourrez alors accéder à l'application en ouvrant votre navigateur et en visitant l'URL http://localhost:8000.

## Conclusion
Ce projet de blog simple est une excellente occasion de me plonger dans le monde de Laravel et de découvrir toutes les fonctionnalités qu'il a à offrir. J'espère que ce readme vous a donné une idée de ce à quoi vous pouvez vous attendre de ce projet et de mon parcours d'apprentissage avec Laravel. N'hésitez pas à explorer et à expérimenter davantage pour approfondir vos connaissances sur Laravel et développer des fonctionnalités plus avancées pour votre blog. Bonne découverte de Laravel !